package test16.composite.better.inheritance;

import java.util.Collection;
import java.util.HashSet;

/**
 * 继承普通类（HashSet不是继承设计的类），将破坏超类的封装性
 * 必须了解所重写函数在超类中的实现细节
 * @param <E>
 */
public class InstrumentedHashSet<E> extends HashSet<E> {
    private int addCount = 0;

    public int getAddCount() {
        return addCount;
    }

    @Override
    public boolean add(E e) {
        this.addCount++;
        return super.add(e);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        this.addCount += c.size();

        // 在超类HashSet中，super.addAll(c)调用了add(E e)函数，add(E e)在
        // 此被重写，这将导致addCount被重复计数
        return super.addAll(c);
    }
}
