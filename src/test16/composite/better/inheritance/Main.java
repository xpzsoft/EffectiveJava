package test16.composite.better.inheritance;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Main {
    public static void main(String args[]) {
        List<String> data = Arrays.asList(new String[]{"1111", "2222"});

        // 直接继承HashSet，重写add和addAll方法导致出错
        InstrumentedHashSet<String> set =new InstrumentedHashSet<String>();
        set.addAll(data);
        // 期望值为2，但实际值为4
        System.out.println(set.getAddCount());

        // 采用复合与转发实现set
        InstrumentedSet<String > set1 = new InstrumentedSet<String>(new HashSet<String>());
        set1.addAll(data);
        // 期望值为2，实际值为2
        System.out.println(set1.getAddCount());
    }
}
