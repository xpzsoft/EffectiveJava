package test16.composite.better.inheritance;

import java.util.Collection;
import java.util.Set;

public class InstrumentedSet<E> extends ForwardSet<E> {
    private int addCount = 0;

    public InstrumentedSet(Set<E> set) {
        super(set);
    }

    @Override
    public int getAddCount() {
        return this.addCount;
    }

    @Override
    public boolean add(E e) {
        this.addCount++;
        return super.add(e);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        this.addCount += c.size();
        return super.addAll(c);
    }
}
