package test16.composite.better.inheritance;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public abstract class ForwardSet<E> implements Set<E> {

    private Set<E> set;

    public ForwardSet(Set<E> set) {
        this.set = set;
    }

    public abstract int getAddCount();

    @Override
    public int size() {
        return this.set.size();
    }

    @Override
    public boolean isEmpty() {
        return this.set.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return this.set.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return this.set.iterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return this.set.toArray(a);
    }

    @Override
    public boolean add(E e) {
        return this.set.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return this.set.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return this.set.addAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return this.set.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return this.set.retainAll(c);
    }

    @Override
    public void clear() {
        this.set.clear();
    }

    @Override
    public String toString() {
        return this.set.toString();
    }

    @Override
    public int hashCode() {
        return this.set.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return this.set.equals(obj);
    }
}
