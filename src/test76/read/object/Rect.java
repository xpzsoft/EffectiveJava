package test76.read.object;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class Rect implements Serializable {
    private  Pointer x;
    private  Pointer y;
    private final int area;

    private Rect(int x1, int y1, int x2, int y2) {
        x = Pointer.instance(x1, y1);
        y = Pointer.instance(x2, y2);
        area = Math.abs(x.getX() - y.getX()) * Math.abs(x.getY() - y.getY());
    }

    public static Rect instance(int x1, int y1, int x2, int y2) {
        return new Rect(x1, y1, x2, y2);
    }

    public Pointer getX() {
        return Pointer.instance(x.getX(), x.getY());
    }

    public Pointer getY() {
        return Pointer.instance(y.getX(), y.getY());
    }

    @Override
    public String toString() {
        return String.format("%s %s area=%d", x.toString(), y.toString(), area);
    }

    // 反序列化保护，防止恶意引用攻击
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        x = Pointer.instance(x.getX(), y.getY());
        y = Pointer.instance(y.getX(), y.getY());
    }
}
