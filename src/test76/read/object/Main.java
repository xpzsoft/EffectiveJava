package test76.read.object;

import java.io.*;

public class Main {
    private static Pointer x = null, y = null;

    public static void main(String args[]) throws IOException, ClassNotFoundException {


        Rect rect = Rect.instance(1,1,2,2);
        System.out.println(rect.toString());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(rect);

        byte[] ref = {0x71, 0, 0x7e, 0, 4};
        bos.write(ref);
        ref[4] = 5;
        bos.write(ref);
        ref[4] = 3;
        bos.write(ref);
        ref[4] = 2;
        bos.write(ref);
        ref[4] = 1;
        bos.write(ref);
        ref[4] = 0;
        bos.write(ref);
        bos.flush();

        System.out.println(bytes2hex(bos.toByteArray()));

        ObjectInputStream in = new ObjectInputStream( new ByteArrayInputStream(bos.toByteArray()));
        rect = (Rect)in.readObject();
        x = (Pointer)in.readObject();
        y = (Pointer) in.readObject();
        Object v3 = in.readObject();
        Object v2 = in.readObject();
        Object v1 = in.readObject();
        Object v0 = in.readObject();
        x.setX(10);
        System.out.println(x);
        System.out.println(y);
        System.out.println(rect.toString());
        System.out.println(v3);
        System.out.println(v2);
        System.out.println(v1);
        System.out.println(v0);

        bos.close();
        out.close();
        in.close();
    }

    public static String bytes2hex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        String tmp = null;
        for (byte b : bytes) {
            // 将每个字节与0xFF进行与运算，然后转化为10进制，然后借助于Integer再转化为16进制
            tmp = Integer.toHexString(0xFF & b);
            if (tmp.length() == 1) {
                tmp = "0" + tmp;
            }
            sb.append(tmp + " ");
        }
        return sb.toString();

    }
}
