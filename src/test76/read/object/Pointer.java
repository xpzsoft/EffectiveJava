package test76.read.object;

import java.io.Serializable;

class Pointer implements Comparable<Pointer>, Serializable {
    private int x;
    private int y;

    private Pointer(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public static Pointer instance(int x, int y) {
        return new Pointer(x, y);
    }

    @Override
    public String toString() {
        return String.format("x=%d, y=%d", x, y);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Pointer)) {
            return false;
        }
        Pointer item = (Pointer) obj;
        return item.x == x && item.y == y;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public int compareTo(Pointer o) {
        if (o.x * o.y == x * y)
            return 0;
        return o.x * o.y < x * y ? 1: -1;
    }
}
